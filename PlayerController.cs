﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class PlayerController : PlayerComponent
{
    #region Fields & Vars
    [Header("Possession")]
    public int controllingController = 1;

    [Header("Inputs")]
    public Vector3 moveInput;
    public Vector3 aimInput;
    public bool jump;
    public bool holdHeight;
    public bool dash;
    public bool throw_;
    public bool useToy;
    public bool continuousUseToy;
    public bool sit;

    [Header("Vars")]
    public bool canPerformActions = true;

    public PlayerManager.PlayerInfos infos;
    #endregion

    private void Update()
    {
        GetMoveAndAimVectors();
        UpdateCanPerformActions();
    }

    private void GetMoveAndAimVectors()
    {
        moveInput = InputManager.GetMove(controllingController);
        aimInput = InputManager.GetAim(controllingController);
        jump = InputManager.playerJumps[controllingController - 1];
        holdHeight = InputManager.playerHoldHeights[controllingController - 1];
        dash = InputManager.playerDashes[controllingController - 1];
        throw_ = InputManager.playerThrows[controllingController - 1];
        useToy = InputManager.playerUseToys[controllingController - 1];
        sit = InputManager.playerSits[controllingController - 1];
        continuousUseToy = InputManager.playerContinuousUseToys[controllingController - 1];
    }

    private void UpdateCanPerformActions()
    {
        if (being.stunned  ||
            being.dead ||
            moves.dashing == true ||
            (interacts.continuouslyUsing && interacts.toyInHand is PickableBunny) ||
            WorldManager.gamePlaying == false)
        {
            canPerformActions = false;
        }
        else
        {
            canPerformActions = true;

        }
    }

    #region Functionalities
    public void Initialize(PlayerManager.PlayerInfos infos, Quaternion startRotation, int respawnLeft)
    {
        controllingController = infos.controllerId;
        this.infos = infos;
        refs.RotatingBody.rotation = startRotation;

        OnInitialize?.Invoke(infos, startRotation, respawnLeft);
    }
    #endregion

    #region Events
    public event System.Action<PlayerManager.PlayerInfos, Quaternion, int>  OnInitialize;

    private void OnValidate()
    {
        if (controllingController < 1)
        {
            controllingController = 1;
        }
    }
    #endregion
}
