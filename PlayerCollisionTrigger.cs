﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class PlayerCollisionTrigger : MonoBehaviour
{
    public PlayerInteractions interacts;

    private void OnCollisionStay(Collision collision)
    {
        PickableObject toy;
        PickableHelmet helmet;
        if (toy = collision.gameObject.GetComponentInParent<PickableObject>())
        {
            if (toy.CanBeOwnedBy(interacts.master) == true)
            {
                interacts.OnToyCollide(toy);
            }
        }
        else if (helmet = collision.gameObject.GetComponentInParent<PickableHelmet>())
        {
            if (helmet.pickable == true)
            {
                interacts.OnHelmetCollide(helmet);
            }
        }
    }
}
