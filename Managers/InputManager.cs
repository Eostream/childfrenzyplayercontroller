﻿/*
Made by: Cossu Michaël
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ControllerType
{
    Null = 0,
    Keyboard = 1,
    PS4 = 2,
    xBox = 3
}

public enum ControllerButton
{
    RightPadDown,
    RightPadRight,
    RightPadUp,
    RightPadLeft,
    LeftPadDown,
    LeftPadRight,
    LeftPadUp,
    LeftPadLeft,
    RightTrigger,
    RightBumper,
    RightMenu,
    RightClick,
    LeftClick,
    LeftMenu,
    LeftBumper,
    LeftTrigger,
    PS,
    Pad
}
public enum ControllerAxis
{
    leftStickHorizontal = 0,
    leftStickVertical = 1,
    rightStickHorizontal = 2,
    rightStickVertical = 3,
    LeftTrigger = 4,
    RightTrigger = 5,
    padHorizontal = 6,
    padVertical = 7
}

public enum ComputerInputType
{
    Mouse = 0,
    Keyboard = 1,
    Wheel = 2
}

[System.Serializable]
public class ComputerInput
{
    public enum WheelDirectionEnum
    {
        Up,
        Down
    }

    [SerializeField] private ComputerInputType _inputType;
    [SerializeField] private InputManager.InputMode _inputMode;
    [SerializeField] private KeyCode _key;
    [SerializeField] private int _mouseButtonId;
    [SerializeField] private WheelDirectionEnum _wheelDirection;

    public ComputerInputType InputType { get { return _inputType; } }
    public KeyCode Key { get { return _key; } }
    public int MouseButtonId { get { return _mouseButtonId; } }
    public WheelDirectionEnum WheelDirection { get { return _wheelDirection; } }

#if UNITY_EDITOR
    public bool Opened;
#endif

    public bool IsPressed()
    {
        switch (_inputType)
        {
            case ComputerInputType.Mouse:
                switch (_inputMode)
                {
                    case InputManager.InputMode.Held:
                        return Input.GetMouseButton(_mouseButtonId);
                    case InputManager.InputMode.Down:
                        return Input.GetMouseButtonDown(_mouseButtonId);
                    case InputManager.InputMode.Up:
                        return Input.GetMouseButtonUp(_mouseButtonId);
                }
                break;
            case ComputerInputType.Keyboard:
                switch (_inputMode)
                {
                    case InputManager.InputMode.Held:
                        return Input.GetKey(_key);
                    case InputManager.InputMode.Down:
                        return Input.GetKeyDown(_key);
                    case InputManager.InputMode.Up:
                        return Input.GetKeyUp(_key);
                }
                break;
            case ComputerInputType.Wheel:
                bool cur = false, last = false;
                switch (_wheelDirection)
                {
                    case WheelDirectionEnum.Up:
                        cur = InputManager.WheelState == InputManager.InputState.Positive;
                        last = InputManager.LastWheelState == InputManager.InputState.Positive;
                        break;
                    case WheelDirectionEnum.Down:
                        cur = InputManager.WheelState == InputManager.InputState.Negative;
                        last = InputManager.LastWheelState == InputManager.InputState.Negative;
                        break;
                }
                switch (_inputMode)
                {
                    case InputManager.InputMode.Held:
                        return cur == true && last == true;
                    case InputManager.InputMode.Down:
                        return cur == true && last == false;
                    case InputManager.InputMode.Up:
                        return cur == false && last == true;
                }
                break;
        }

        return false;
    }
}

public class InputManager : Singleton<InputManager>
{
    public enum StickSide
    {
        Left,
        Right
    }
    public enum InputMode
    {
        Held = 0,
        Down = 1,
        Up = 2
    }
    public enum InputState
    {
        None,
        Positive,
        Negative
    }
    
    public string[] _controllerNames;
    public ControllerType[] _controllerTypes;

    private static List<InputState[]> _lastAxisInput;
    private static List<InputState[]> _axisInput;

    #region Inputs
    #endregion

    public static InputState WheelState { get { return _wheelState; } }
    public static InputState LastWheelState { get { return _lastWheelState; } }

    private static float _wheelAxis;
    private static InputState _wheelState;
    private static InputState _lastWheelState;

    public static event System.Action OnChangedController;

    public Vector2[] playerMoveInputs;
    public Vector2[] playerAimInputs;

    public static bool[] playerJumps;
    public static bool[] playerHoldHeights;
    public static bool[] playerDashes;
    public static bool[] playerThrows;
    public static bool[] playerUseToys;
    public static bool[] playerSits;
    public static bool[] playerContinuousUseToys;

    protected override void Awake()
    {
        base.Awake();
        _lastAxisInput = new List<InputState[]>();
        _axisInput = new List<InputState[]>();

        _controllerNames = new string[0];
        _controllerTypes = new ControllerType[0];
        ComputeControllerTypes();

        playerMoveInputs = new Vector2[4];
        playerAimInputs = new Vector2[4];
        playerJumps = new bool[4];
        playerHoldHeights = new bool[4];
        playerDashes = new bool[4];
        playerThrows = new bool[4];
        playerUseToys = new bool[4];
        playerSits = new bool[4];
        playerContinuousUseToys = new bool[4];
    }

    private void Update()
    {
        CheckNewControllers();
        ComputeAxisToButton();
        ComputeMoveAndAimInputs();
        ComputeActionsInputs();
    }

    private void CheckNewControllers()
    {
        if (_controllerNames.Length < Input.GetJoystickNames().Length)
        {
            OnChangedController?.Invoke();
            ComputeControllerTypes();
        }
    }
    private void ComputeAxisToButton()
    {
        //Wheel
        _lastWheelState = _wheelState;
        _wheelAxis = Input.GetAxis("Mouse ScrollWheel");
        _wheelState = (_wheelAxis > 0.8f ? InputState.Positive : (_wheelAxis < -0.8f ? InputState.Negative : InputState.None));

        //Controllers
        StoreLastAxisInput();
        _axisInput.Clear();

        float axisValue;
        for (int controllerId = 1; controllerId <= _controllerTypes.Length; controllerId++)
        {
            _axisInput.Add(new InputState[8]);

            for (int axisId = 0; axisId < 8; axisId++)
            {
                axisValue = GetControllerAxis((ControllerAxis)axisId, controllerId, _controllerTypes[controllerId - 1]);
                _axisInput[controllerId - 1][axisId] = 
                    (axisValue > 0.8f ? InputState.Positive :
                    (axisValue < -0.8f ? InputState.Negative :
                    InputState.None));
            }
        }


    }
    private void StoreLastAxisInput()
    {
        _lastAxisInput.Clear();
        for (int i = 0; i < _axisInput.Count; i++)
        {
            _lastAxisInput.Add(_axisInput[i]);
        }
    }

    private void ComputeMoveAndAimInputs()
    {
        for (int i = 0; i < playerMoveInputs.Length; i++)
        {
            if (i >= _controllerTypes.Length)
            {
                break;
            }

            Vector2 input;
            input =
                Utilities.DeadzoneVector(
                new Vector2(
                GetControllerAxis(ControllerAxis.leftStickHorizontal, i + 1, _controllerTypes[i]),
                GetControllerAxis(ControllerAxis.leftStickVertical, i + 1, _controllerTypes[i])
                ), 0.3f, 0.1f, 1.5f);

            playerMoveInputs[i] = input;

            input =
                Utilities.DeadzoneVector(
                new Vector2(
                GetControllerAxis(ControllerAxis.rightStickHorizontal, i + 1, _controllerTypes[i]),
                GetControllerAxis(ControllerAxis.rightStickVertical, i + 1, _controllerTypes[i])
                ), 0.15f, 0.1f, 1.5f);

            playerAimInputs[i] = input;
        }
    }

    private void ComputeActionsInputs()
    {
        for (int i = 0; i < playerMoveInputs.Length; i++)
        {
            if (i >= _controllerTypes.Length)
            {
                break;
            }

            playerJumps[i] = GetControllerButton(ControllerButton.RightPadDown, i + 1, InputMode.Down, _controllerTypes[i]);
            playerHoldHeights[i] = GetControllerButton(ControllerButton.RightPadDown, i + 1, InputMode.Held, _controllerTypes[i]);
            playerDashes[i] = GetControllerButton(ControllerButton.LeftBumper, i + 1, InputMode.Down, _controllerTypes[i]);
            playerThrows[i] = GetControllerButton(ControllerButton.RightPadUp, i + 1, InputMode.Down, _controllerTypes[i]);
            playerUseToys[i] = GetControllerButton(ControllerButton.RightBumper, i + 1, InputMode.Down, _controllerTypes[i]);
            playerSits[i] = GetControllerButton(ControllerButton.RightBumper, i + 1, InputMode.Down, _controllerTypes[i]);
            playerContinuousUseToys[i] = GetControllerButton(ControllerButton.RightBumper, i + 1, InputMode.Held, _controllerTypes[i]);
        }
    }

    private void ComputeControllerTypes()
    {
        _controllerNames = Input.GetJoystickNames();
        _controllerTypes = new ControllerType[_controllerNames.Length];

        for (int i = 0; i < _controllerNames.Length; i++)
        {
            _controllerTypes[i] = ComputeControllerType(i + 1);
        }
    }
    private static ControllerType ComputeControllerType(int id)
    {
        if (id == 0)
        {
            return ControllerType.Keyboard;
        }

        string[] joystickNames = Input.GetJoystickNames();

        switch (joystickNames[id - 1].Length)
        {
            case 19:
                return ControllerType.PS4;
            case 33:
                return ControllerType.xBox;
        }

        return ControllerType.xBox;
    }
    private static ControllerType GetControllerType(int id)
    {
        if (id == 0)
        {
            return ControllerType.Keyboard;
        }

        if (id > Instance._controllerTypes.Length)
        {
            return ControllerType.Null;
        }

        return Instance._controllerTypes[id - 1];
    }
    public static Vector2 GetMove(int player)
    {
        return Instance.playerMoveInputs[player - 1];
    }
    public static Vector2 GetAim(int player)
    {
        return Instance.playerAimInputs[player - 1];
    }


    delegate bool GetKey(KeyCode key);
    delegate bool GetMouseButton(int button);
    public static bool GetControllerButton(ControllerButton button, int id, InputMode inputMode = InputMode.Down, ControllerType type = ControllerType.Null)
    {
        GetKey getKey;
        if (inputMode == InputMode.Held)
            getKey = Input.GetKey;
        else if (inputMode == InputMode.Up)
            getKey = Input.GetKeyUp;
        else
            getKey = Input.GetKeyDown;

        switch (type == ControllerType.Null ? GetControllerType(id) : type)
        {
            case ControllerType.PS4:
                switch (button)
                {
                    case ControllerButton.RightPadDown:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button1"));
                    case ControllerButton.RightPadRight:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button2"));
                    case ControllerButton.RightPadUp:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button3"));
                    case ControllerButton.RightPadLeft:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button0"));
                    case ControllerButton.RightTrigger:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button7"));
                    case ControllerButton.RightBumper:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button5"));
                    case ControllerButton.RightMenu:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button9"));
                    case ControllerButton.RightClick:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button11"));
                    case ControllerButton.LeftClick:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button10"));
                    case ControllerButton.LeftMenu:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button8"));
                    case ControllerButton.LeftBumper:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button4"));
                    case ControllerButton.LeftTrigger:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button6"));
                    case ControllerButton.PS:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button12"));
                    case ControllerButton.Pad:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button13"));

                }
                break;
            case ControllerType.xBox:
                switch (button)
                {
                    case ControllerButton.RightPadDown:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button0"));
                    case ControllerButton.RightPadRight:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button1"));
                    case ControllerButton.RightPadUp:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button3"));
                    case ControllerButton.RightPadLeft:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button2"));
                    case ControllerButton.RightBumper:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button5"));
                    case ControllerButton.RightMenu:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button7"));
                    case ControllerButton.RightClick:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button9"));
                    case ControllerButton.LeftClick:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button8"));
                    case ControllerButton.LeftMenu:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button6"));
                    case ControllerButton.LeftBumper:
                        return getKey((KeyCode)System.Enum.Parse(typeof(KeyCode), "Joystick" + id + "Button4"));
                    case ControllerButton.RightTrigger:
                        return GetControllerAxis(ControllerAxis.RightTrigger, id, inputMode);
                    case ControllerButton.LeftTrigger:
                        return GetControllerAxis(ControllerAxis.LeftTrigger, id, inputMode);
                    case ControllerButton.PS:
                        return false;
                    case ControllerButton.Pad:
                        return false;
                }
                break;

        }
        switch (button)
        {
            case ControllerButton.LeftPadDown:
                return GetControllerAxis(ControllerAxis.padVertical, id, inputMode, false);
            case ControllerButton.LeftPadUp:
                return GetControllerAxis(ControllerAxis.padVertical, id, inputMode, true);
            case ControllerButton.LeftPadLeft:
                return GetControllerAxis(ControllerAxis.padHorizontal, id, inputMode, false);
            case ControllerButton.LeftPadRight:
                return GetControllerAxis(ControllerAxis.padHorizontal, id, inputMode, true);
        }

        return false;
    }

    public static float GetControllerAxis(ControllerAxis axis, int id, ControllerType type = ControllerType.Null)
    {
        switch (type == ControllerType.Null ? GetControllerType(id) : type)
        {
            case ControllerType.PS4:
                switch (axis)
                {
                    case ControllerAxis.leftStickHorizontal:
                        return Input.GetAxis("Joystick" + id + "AxisX");
                    case ControllerAxis.leftStickVertical:
                        return Input.GetAxis("Joystick" + id + "AxisY") * -1;
                    case ControllerAxis.rightStickHorizontal:
                        return Input.GetAxis("Joystick" + id + "Axis3");
                    case ControllerAxis.rightStickVertical:
                        return Input.GetAxis("Joystick" + id + "Axis6") * -1;
                    case ControllerAxis.LeftTrigger:
                        return Input.GetAxis("Joystick" + id + "Axis4");
                    case ControllerAxis.RightTrigger:
                        return Input.GetAxis("Joystick" + id + "Axis5");
                    case ControllerAxis.padHorizontal:
                        return Input.GetAxis("Joystick" + id + "Axis7");
                    case ControllerAxis.padVertical:
                        return Input.GetAxis("Joystick" + id + "Axis8");
                    default:
                        break;
                }
                break;
            case ControllerType.xBox:
                switch (axis)
                {
                    case ControllerAxis.leftStickHorizontal:
                        return Input.GetAxis("Joystick" + id + "AxisX");
                    case ControllerAxis.leftStickVertical:
                        return Input.GetAxis("Joystick" + id + "AxisY") * -1;
                    case ControllerAxis.rightStickHorizontal:
                        return Input.GetAxis("Joystick" + id + "Axis4");
                    case ControllerAxis.rightStickVertical:
                        return Input.GetAxis("Joystick" + id + "Axis5") * -1;
                    case ControllerAxis.LeftTrigger:
                        return (Input.GetAxis("Joystick" + id + "Axis9") - 0.5f) * 2f;
                    case ControllerAxis.RightTrigger:
                        return (Input.GetAxis("Joystick" + id + "Axis10") - 0.5f) * 2f;
                    case ControllerAxis.padHorizontal:
                        return Input.GetAxis("Joystick" + id + "Axis6");
                    case ControllerAxis.padVertical:
                        return Input.GetAxis("Joystick" + id + "Axis7");
                    default:
                        break;
                }
                break;
        }
        return 0f;
    }
    public static bool GetControllerAxis(ControllerAxis axis, int id, InputMode InputMode = InputMode.Held, bool positiveComparison = true)
    {
        if (_lastAxisInput.Count == 0)
        {
            return false;
        }

        if (positiveComparison)
        {
            switch (InputMode)
            {
                case InputMode.Held://held
                    return _axisInput[id - 1][(int)axis] == InputState.Positive;
                case InputMode.Down://down
                    return _axisInput[id - 1][(int)axis] == InputState.Positive && _lastAxisInput[id - 1][(int)axis] != InputState.Positive;
                case InputMode.Up://u
                    return _axisInput[id - 1][(int)axis] != InputState.Positive && _lastAxisInput[id - 1][(int)axis] == InputState.Positive;
            }
        }
        else
        {
            switch (InputMode)
            {
                case InputMode.Held://held
                    return _axisInput[id - 1][(int)axis] == InputState.Negative;
                case InputMode.Down://down
                    return _axisInput[id - 1][(int)axis] == InputState.Negative && _lastAxisInput[id - 1][(int)axis] != InputState.Negative;
                case InputMode.Up://up
                    return _axisInput[id - 1][(int)axis] != InputState.Negative && _lastAxisInput[id - 1][(int)axis] == InputState.Negative;
            }
        }


        throw new System.NotImplementedException();
    }
}
