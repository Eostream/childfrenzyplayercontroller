﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public enum AliveMode
{
    Alive,
    Stunned,
    Dead
}

public class PlayerBeing : PlayerComponent
{
    #region Fields & Vars
    [Header("Params")]
    [Space]
    public float maxHealth;

    [Header("Take Hit Stun")]
    public float stunTime;
    public float invincibilityTime;

    public int respawnLeft = 3;
    public bool hasHelmet = false;
    public PickableHelmet helmet;

    public float currentHealth;
    [System.NonSerialized] public AliveMode aliveMode = AliveMode.Alive;
    [System.NonSerialized] public bool invicible = false;

    public bool dead { get { return aliveMode == AliveMode.Dead; } }
    public bool stunned { get { return aliveMode == AliveMode.Stunned; } }
    public float healthProgress { get { return currentHealth / maxHealth; } }
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();
        currentHealth = maxHealth;
        master.OnInitialize += OnInitialize;
    }

    private void OnDisable()
    {
        master.OnInitialize -= OnInitialize;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            TakeHit(new HitInfos(master, 10));
        }
    }
    #endregion

    #region Functionalities
    public bool AddHealth(float amount)
    {
        currentHealth += amount;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        else if (currentHealth <= 0f)
        {
            currentHealth = 0f;

            OnHealthChange?.Invoke();
            return true;
        }

        OnHealthChange?.Invoke();
        return false;
    }
    public void TakeHit(HitInfos infos)
    {
        if (invicible == true || WorldManager.gamePlaying == false)
        {
            return;
        }

        CameraShake.Shake(10f);

        if (hasHelmet == true)
        {
            hasHelmet = false;
            helmet.DetachByHit(infos);
            helmet = null;

            OnTakeHitOnHelmet?.Invoke(infos);
        }
        else
        {
            invicible = true;
            aliveMode = AliveMode.Stunned;
            Invoke("RemoveStun", infos.stunTime);
            Invoke("RemoveInvincibility", infos.invincibilityTime);

            if (AddHealth(-infos.damage))
            {
                Die(infos);
            }

            OnTakeHit?.Invoke(infos);
        }
    }
    private void RemoveStun()
    {
        if (aliveMode == AliveMode.Stunned)
        {
            aliveMode = AliveMode.Alive;
        }
    }
    private void RemoveInvincibility()
    {
        invicible = false;
    }
    public void AttachHelmet(PickableHelmet helmet)
    {
        helmet.Attach(master, visuals.socketsRefs.GetSocketHead());
        hasHelmet = true;
        this.helmet = helmet;

        OnAttachHelmet?.Invoke();
    }

    public void Die(HitInfos killingHit)
    {
        aliveMode = AliveMode.Dead;

        OnDie?.Invoke(killingHit);

        WorldManager.OnPlayerDie(master);
    }
    #endregion

    #region Events
    public event System.Action<HitInfos> OnTakeHit;
    public event System.Action<HitInfos> OnTakeHitOnHelmet;
    public event System.Action<HitInfos> OnDie;
    public event System.Action OnInvicibleEnd;
    public event System.Action OnAttachHelmet;
    public event System.Action OnHealthChange;

    private void OnInitialize(PlayerManager.PlayerInfos infos, Quaternion startRotation, int respawnLeft)
    {
        this.respawnLeft = respawnLeft;
    }
    #endregion
}
