﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class PlayerAim : PlayerComponent
{
    #region Fields & Vars
    public Transform HandsTransform;
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();
        visuals.OnModelChange += OnModelChange;
    }

    private void Update()
    {
        if (WorldManager.gamePlaying == false)
            return;

        if (interacts.hasToy == false ||
            interacts.toyInHand is PickableSword ||
            interacts.toyInHand is PickableBunny)
        {
            HandsTransform.localRotation = Quaternion.identity;
        }
        else
        {
            float rad = Mathf.Atan2(master.aimInput.x, master.aimInput.y);
            HandsTransform.rotation = Quaternion.Euler(0, (rad * (180f / Mathf.PI)), 0);
        }

    }
    #endregion

    #region Events
    private void OnModelChange()
    {
        HandsTransform = visuals.socketsRefs.GetSocketHandLeft().parent.parent;
    }
    #endregion
}
