﻿using UnityEngine;

public class PlayerReferences : PlayerComponent
{
    public Transform Body;
    public Rigidbody Rb;
    public CapsuleCollider Coll;
    public Animator Anim;
    public Transform Visual;
    public Transform RotatingBody;
    public BoxCollider attackHitbox;
    public BoxCollider toyHitbox;
}
