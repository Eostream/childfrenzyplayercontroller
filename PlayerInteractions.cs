﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/


using UnityEngine;

public class PlayerInteractions : PlayerComponent
{
    #region Fields & Vars
    [Header("Throw force")]
    public float throwHorizontalSpeed;
    public float throwVerticalSpeed;

    [Header("Loose Item by hit")]
    public float looseHorizontalSpeed;
    public float looseVerticalSpeed;

    [Header("Attack")]
    public float damage;
    public float attackStunTime = 0.4f;
    public float attackInvincibilityTime = 0.6f;
    public float knockbackForce;
    public float attackCooldown = 0.33f;

    [Header("Vars")]
    public bool hasToy = false;
    public PickableObject toyInHand = null;
    public bool continuouslyUsing = false;

    private float attackLastTime = Mathf.NegativeInfinity;
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();
        being.OnTakeHit += OnTakeHit;
        being.OnDie += OnDie;
    }

    private void OnDisable()
    {
        being.OnTakeHit -= OnTakeHit;
        being.OnDie -= OnDie;
    }

    private void Update()
    {
        if (master.canPerformActions == false && continuouslyUsing == false)
        {
            return;
        }
        else if (WorldManager.gamePlaying == false)
        {
            return;
        }

        if (hasToy == true)
        {
            if (master.throw_ == true)
            {
                ThrowToy();
            }

            if (master.useToy == true)
            {
                UseToy();
            }

            if (toyInHand != null && master.continuousUseToy)
            {
                if (continuouslyUsing == false)
                {
                    OnContinuousUseToyStart?.Invoke();
                }
                continuouslyUsing = true;
                ContinuousUseToy();
            }
            else if (continuouslyUsing == true)
            {
                continuouslyUsing = false;
                OnContinuousUseToyEnd?.Invoke();
            }
        }
        else
        {
            if (master.useToy == true && attackLastTime + attackCooldown < Time.time)
            {
                Attack();
            }

            if (continuouslyUsing == true)
            {
                continuouslyUsing = false;
                OnContinuousUseToyEnd?.Invoke();
            }
        }
    }
    #endregion
    #region Functionalities
    private void AttachToy(PickableObject toy)
    {
        this.toyInHand = toy;
        hasToy = true;
        toyInHand.PickUp(master, visuals.socketsRefs.GetSocketHandLeft());

        OnTakeToy?.Invoke();
    }
    private void ThrowToy()
    {
        PickableObject toy = this.toyInHand;
        DetachToy();
        toy.Throw((moves.lastMoveDirection.normalized * throwHorizontalSpeed).SetY(throwVerticalSpeed));

        OnThrowToy?.Invoke();
    }
    private void LooseToyByHit(Vector3 direction)
    {
        PickableObject toy = this.toyInHand;
        DetachToy();

        toy.Throw((direction * looseHorizontalSpeed).SetY(looseVerticalSpeed));
    }
    public void DetachToy()
    {
        hasToy = false;
        toyInHand.Detach();
        toyInHand = null;

        OnDetachToy?.Invoke();
        anims.OnStopPickUpToy();
    }
    private void UseToy()
    {
        toyInHand.Use();
        OnUseToy?.Invoke();
    }
    private void ContinuousUseToy()
    {
        toyInHand.ContinuousUse();
        OnContinuousUseToy?.Invoke();
    }
    private void Attack()
    {
        attackLastTime = Time.time;
        OnAttack?.Invoke();
        Invoke("CheckAttackHitbox", 0.05f);
    }

    private void CheckAttackHitbox()
    {
        HitInfos hit = new HitInfos(
            master,
            damage,
            default,
            knockbackForce,
            attackStunTime,
            attackInvincibilityTime,
            true,
            false);

        CheckHitbox(hit, refs.attackHitbox);
    }
    public bool CheckHitbox(HitInfos hit, BoxCollider hitbox)
    {
        bool toreturn = false;
        Collider[] colls = Physics.OverlapBox(
            hitbox.transform.position,
            hitbox.transform.localScale * 0.5f,
            hitbox.transform.rotation,
            WorldManager.AttackMask);

        for (int i = 0; i < colls.Length; i++)
        {
            PickableObject o;

            if (o = colls[i].GetComponentInParent<PickableObject>())
            {
                if (o.owner != null)
                {
                    continue;
                }
                hit.direction = (o.Position - Position).normalized;
                o.Throw(hit.direction * hit.knockbackForce);
                continue;
            }

            PickableHelmet h;
            if (h = colls[i].GetComponentInParent<PickableHelmet>())
            {
                if (h.pickable == false)
                {
                    continue;
                }

                hit.direction = (h.rb.position - Position).normalized;
                h.rb.velocity = (hit.direction * hit.knockbackForce);
            }

            PlayerController player;
            if (player = colls[i].GetComponentInParent<PlayerController>())
            {
                if (player == master)
                {
                    continue;
                }
                toreturn = true;

                hit.direction = (player.Position - Position).normalized;
                player.being.TakeHit(hit);
                continue;
            }
        }

        return toreturn;
    }
    #endregion

    #region Events
    public event System.Action OnTakeToy;
    public event System.Action OnThrowToy;
    public event System.Action OnDetachToy;
    public event System.Action OnUseToy;
    public event System.Action OnContinuousUseToyStart;
    public event System.Action OnContinuousUseToy;
    public event System.Action OnContinuousUseToyEnd;
    public event System.Action OnAttack;

    public void OnToyCollide(PickableObject toy)
    {
        if (hasToy == true || Time.timeSinceLevelLoad < 0.2f)
        {
            return;
        }

        AttachToy(toy);
    }
    public void OnHelmetCollide(PickableHelmet helmet)
    {
        if (being.hasHelmet == true || Time.timeSinceLevelLoad < 0.2f)
        {
            return;
        }
        being.AttachHelmet(helmet);
    }
    private void OnTakeHit(HitInfos infos)
    {
        if (infos.canDisarm == true && hasToy == true)
        {
            LooseToyByHit(infos.direction);
        }
    }
    private void OnDie(HitInfos killingHit)
    {
        if (hasToy == true)
        {
            PickableObject toy = toyInHand;
            DetachToy();
            toy.Throw(killingHit.direction * killingHit.knockbackForce);
        }
    }
    #endregion
}
