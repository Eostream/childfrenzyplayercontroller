﻿/*
Made by: Allan Arnaudin, Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

public class PlayerAnimations : PlayerComponent
{
    #region Constants
    private const string AttackNameState = "CharacterAttack";
    private const string AttackObjectNameState = "CharacterAttackWithObject";   
    private const string DashNameState = "CharacterDash";
    private const string MoveNameState = "Move";
    private const string HoldNameState = "Hold";
    private const string ThrowNameState = "CharacterThrow";
    private const string WinNameState = "Win";
    private const string NoneNameState = "None";
    #endregion

    #region Fields & Vars
    private bool continuouslyUsing = false;
    #endregion

    #region MonoBehaviour
    protected void OnEnable()
    {
        moves.OnJump += OnJump;
        moves.OnDash += OnDash;
        interacts.OnThrowToy += OnThrowToy;
        interacts.OnDetachToy += OnThrowToy;
        interacts.OnTakeToy += OnPickUpToy;
        interacts.OnAttack += OnAttack;
        interacts.OnContinuousUseToyStart += OnContinuousToyUseStart;
        interacts.OnContinuousUseToyEnd += OnContinuousUseToyEnd;

        WorldManager.OnGameEnd += OnGameEnd;
    }

    protected void OnDisable()
    {
        moves.OnJump -= OnJump;
        moves.OnDash -= OnDash;
        interacts.OnThrowToy -= OnThrowToy;
        interacts.OnDetachToy -= OnThrowToy;
        interacts.OnTakeToy -= OnPickUpToy;
        interacts.OnAttack -= OnAttack;
        interacts.OnContinuousUseToyStart -= OnContinuousToyUseStart;
        interacts.OnContinuousUseToyEnd -= OnContinuousUseToyEnd;

        WorldManager.OnGameEnd -= OnGameEnd;
    }

    private void Update()
    {
        UpdateVariables();

        if (interacts.hasToy == true)
        {
            if (continuouslyUsing == false)
            {
                refs.Anim.Play(HoldNameState, 2, 0);
            }
            else
            {
                refs.Anim.Play("None", 2, 0);
            }
        }
    }

    private void UpdateVariables()
    {
        refs.Anim.SetBool("grounded", moves.grounded);
        refs.Anim.SetFloat("hSpeed", Velocity.SetY(0).magnitude);
    }
    #endregion

    #region Events
    private void OnJump()
    {

    }

    private void OnContinuousToyUseStart()
    {
        if (interacts.toyInHand is PickableBunny)
        {
            refs.Anim.CrossFadeInFixedTime("CharacterHug", 0.2f);
        }
    }

    private void OnContinuousUseToyEnd()
    {
        refs.Anim.CrossFadeInFixedTime("Move", 0.2f);
    }

    private void OnDash()
    {
        refs.Anim.Play(DashNameState, 1, 0);
    }

    private void OnPickUpToy()
    {
        refs.Anim.Play(HoldNameState, 2, 0);
    }
    public void OnStopPickUpToy()
    {
        refs.Anim.Play(NoneNameState, 2, 0);
    }

    private void OnThrowToy()
    {
        refs.Anim.Play(ThrowNameState, 5, 0);
    }
    private void OnAttack()
    {
        refs.Anim.Play(AttackNameState, 3, 0);
    }
    public void OnAttackWithObject()
    {
        refs.Anim.Play(AttackObjectNameState, 4, 0);
    }
    private void OnGameEnd(PlayerController playerController)
    {
        refs.Anim.Play(WinNameState, 6, 0);
    }
    #endregion
}
