﻿/*
Made by: Allan Arnaudin
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using System.Collections;
using UnityEngine;

public class PlayerVisual : PlayerComponent
{
    #region Fields & Vars
    [SerializeField] private Transform bodyTransform;
    public SocketsReferences socketsRefs;

    [Header("Visual Attack")]
    [SerializeField] private GameObject fxAttack;

    [Header("Jump")]
    [SerializeField] private GameObject fxJump;
    private Vector3 offsetJumpFX;

    [Header("Visual Dash")]
    [SerializeField] private GameObject fxDash;
    [SerializeField] private float offsetFXDashDuration = 0.2f;
    private GameObject currentDashFX;
    private Coroutine coroutineDeleteFXDash;
    private YieldInstruction waitForEndOfFXDash;
    private YieldInstruction offsetWaitFXDashDuration;

    [Header("Visual Ghost")]
    [SerializeField] private GameObject fxGhost;

    [Header("Visual Stun")]
    [SerializeField] private GameObject fxTakeHit;
    [SerializeField] private GameObject fxStun;
    private Vector3 offsetHitFX;
    private GameObject currentStunFX;
    private Coroutine coroutineDeleteFXStun;

    [Header("Visual Die")]
    [SerializeField] private GameObject fxDie;


    public Material normalMat;
    public Material transparentMat;

    private Mesh headMesh;
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();

        //Jump
        offsetJumpFX = new Vector3(0, 0.2f, 0);

        //Dash
        currentDashFX = null;
        waitForEndOfFXDash = new WaitForSeconds(1f);
        offsetWaitFXDashDuration = new WaitForSeconds(offsetFXDashDuration);
        coroutineDeleteFXDash = null;

        //Stun
        offsetHitFX = new Vector3(0, refs.Coll.height * 0.5f, 0);
        currentStunFX = null;
        coroutineDeleteFXStun = null;
    }

    protected void OnEnable()
    {
        //Listeners
        moves.OnJump += OnJump;
        moves.OnDash += OnDash;
        moves.OnDashEnd += OnDashEnd;
        being.OnTakeHit += OnTakeHit;
        interacts.OnAttack += OnAttack;
        being.OnDie += OnDie;
        being.OnInvicibleEnd += OnInvincibleEnd;
        master.OnInitialize += OnInitialize;
        being.OnAttachHelmet += OnAttachHelmet;
        being.OnTakeHitOnHelmet += OnTakeHitOnHelmet;

        WorldManager.OnGameEnd += FinishAGame;
    }

    protected void OnDisable()
    {
        moves.OnJump -= OnJump;
        moves.OnDash -= OnDash;
        moves.OnDashEnd -= OnDashEnd;
        being.OnTakeHit -= OnTakeHit;
        interacts.OnAttack -= OnAttack;
        being.OnDie -= OnDie;
        being.OnInvicibleEnd -= OnInvincibleEnd;
        master.OnInitialize -= OnInitialize;
        being.OnAttachHelmet -= OnAttachHelmet;
        being.OnTakeHitOnHelmet -= OnTakeHitOnHelmet;

        WorldManager.OnGameEnd -= FinishAGame;
    }

    private void Update()
    {
        ApplyRotation();
    }
    private void ApplyRotation()
    {
        Rotation = Quaternion.LookRotation(moves.lastMoveDirection);
    }
    #endregion

    #region Functionalities
    public void SetPlayerModel(PlayerManager.PlayerInfos infos)
    {
        Transform newVisual = Instantiate(infos.playerVisualModel, refs.Visual.position, refs.Visual.rotation, refs.Visual.parent).transform;
        Destroy(refs.Visual.gameObject);
        refs.Visual = newVisual;
        refs.Anim = newVisual.GetComponent<Animator>();
        socketsRefs = newVisual.GetComponent<SocketsReferences>();

        OnModelChange?.Invoke();
    }


    private void FinishAGame(PlayerController playerController)
    {
        StartCoroutine(BoomParty(playerController));
    }
    private IEnumerator BoomParty(PlayerController playerController)
    {
        if (!fxDie)
            yield break;

        while (true)
        {
            InstantiateFXBoomRandom(playerController);
            yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
        }
    }
    private void InstantiateFXBoomRandom(PlayerController playerController)
    {
        float randomPosOffsetX = Random.Range(-10f, 10f);
        float randomPosOffsetZ = Random.Range(-10f, 10f);
        Instantiate(fxDie, playerController.visuals.socketsRefs.GetSocketBodyDownRear().position + new Vector3(randomPosOffsetX, 0, randomPosOffsetZ), Quaternion.identity);
    }
    #endregion

    #region Events
    public event System.Action OnModelChange;
    private void OnInitialize(PlayerManager.PlayerInfos infos, Quaternion startRotation, int respawnLeft)
    {
        SetPlayerModel(infos);
    }
    private void OnJump()
    {
        if (fxJump)
            Instantiate(fxJump, bodyTransform.position + offsetJumpFX, Quaternion.identity);
    }
    #region OnDash
    private void OnDash()
    {
        if (coroutineDeleteFXDash != null)
        {
            StopCoroutine(coroutineDeleteFXDash);
            coroutineDeleteFXDash = null;
            Destroy(currentDashFX);
        }

        if (fxDash)
        {
            currentDashFX = Instantiate(fxDash, socketsRefs.GetSocketBodyDownRear().position, Quaternion.identity, socketsRefs.GetSocketBodyDownRear());
            currentDashFX.transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
    }

    private void OnDashEnd()
    {
        if (currentDashFX)
        {
            if (coroutineDeleteFXDash == null)
                coroutineDeleteFXDash = StartCoroutine(waitToDeleteFXDash());
        }

    }

    private IEnumerator waitToDeleteFXDash()
    {
        yield return offsetWaitFXDashDuration;
        currentDashFX.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        yield return waitForEndOfFXDash;
        if (currentDashFX)
            Destroy(currentDashFX);
        coroutineDeleteFXDash = null;
    }
    #endregion
    private void OnAttack()
    {
        if (fxAttack)
            Instantiate(fxAttack, visuals.socketsRefs.GetSocketHandRight().position, Quaternion.identity, visuals.socketsRefs.GetSocketHandRight());
    }
    #region TakeHit
    private void OnTakeHit(HitInfos infos)
    {
        if (fxTakeHit)
            Instantiate(fxTakeHit, bodyTransform.position + offsetHitFX, Quaternion.identity);

        if (currentStunFX == null)
        {
            if (fxStun)
                currentStunFX = Instantiate(fxStun, socketsRefs.GetSocketUpHead().position, Quaternion.identity, socketsRefs.GetSocketUpHead());
        }
        if (coroutineDeleteFXStun != null)
            StopCoroutine(coroutineDeleteFXStun);
        coroutineDeleteFXStun = StartCoroutine(WaitEndOfStun());
    }

    private IEnumerator WaitEndOfStun()
    {
        yield return new WaitForSeconds(being.stunTime);
        Destroy(currentStunFX);
        currentStunFX = null;
        coroutineDeleteFXStun = null;
    }
    #endregion
    private void OnControlTaken()
    {
        if (fxGhost)
            Instantiate(fxGhost, bodyTransform.position, Quaternion.identity);
    }
    private void OnDie(HitInfos killingHit)
    {
        if (fxDie)
            Instantiate(fxDie, bodyTransform.position + offsetHitFX, Quaternion.identity);
    }



    private void OnInvincibleEnd()
    {
        foreach (MeshRenderer r in refs.Visual.GetComponentsInChildren<MeshRenderer>())
        {

        }
    }

    private void OnStartControl()
    {
        refs.Visual.gameObject.SetActive(false);
    }
    private void OnLooseControl()
    {
        refs.Visual.gameObject.SetActive(true);
    }

    private void OnAttachHelmet()
    {
        headMesh = socketsRefs.Head.GetComponent<MeshFilter>().mesh;
        socketsRefs.Head.GetComponent<MeshFilter>().mesh = null;
    }

    private void OnTakeHitOnHelmet(HitInfos infos)
    {
        socketsRefs.Head.GetComponent<MeshFilter>().mesh = headMesh;
    }

    #endregion
}
