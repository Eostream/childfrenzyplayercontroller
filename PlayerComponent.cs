﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class PlayerComponent : MonoBehaviour
{
    [System.NonSerialized] public PlayerReferences refs;
    [System.NonSerialized] public PlayerController master;
    [System.NonSerialized] public PlayerBeing being;
    [System.NonSerialized] public PlayerMovements moves;
    [System.NonSerialized] public PlayerInteractions interacts;
    [System.NonSerialized] public PlayerVisual visuals;
    [System.NonSerialized] public PlayerSound sounds;
    [System.NonSerialized] public PlayerAnimations anims;

    public PlayerController Master { get { return master; } }

    public Vector3 Position { get { return refs.Body.position; } set { refs.Rb.MovePosition(value); } }
    public Quaternion Rotation { get { return refs.RotatingBody.localRotation; } set { refs.RotatingBody.localRotation = value; } }
    public Vector3 Velocity { get { return refs.Rb.velocity; } set { refs.Rb.velocity = value; } }

    public bool HasToy { get { return interacts.hasToy; } }

    protected bool componentInitialized = false;

    protected virtual void Awake()
    {
        InitializeComponent();
    }

    protected void InitializeComponent()
    {
        refs = GetComponent<PlayerReferences>();
        master = GetComponent<PlayerController>();
        being = GetComponent<PlayerBeing>();
        moves = GetComponent<PlayerMovements>();
        interacts = GetComponent<PlayerInteractions>();
        visuals = GetComponent<PlayerVisual>();
        sounds = GetComponent<PlayerSound>();
        anims = GetComponent<PlayerAnimations>();
    }
}
