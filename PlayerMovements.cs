﻿/*
Made by: Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using System.Collections;
using UnityEngine;

public class PlayerMovements : PlayerComponent
{
    #region Fields & Vars
    [Header("Speed")]
    public float acceleration;
    public float airAcceleration;
    public float normalMaxSpeed;
    public float chairMaxSpeed;
    public float brakeForce;
    public float ghostUpDownSpeed = 4;
    public float ghostBrakeForce;


    [Header("Take Hit Knockback")]
    public float knockbackSpeed;
    public float noBrakeTime = 0.4f;

    [Header("Jump Dash")]
    public float jumpSpeed;
    public float jumpCooldown;
    public float dashSpeed;
    public float dashDuration;
    public float dashCooldown;

    [Header("GroundDetection")]
    public float startY;
    public float rayDistance;

    [Header("Vars")]
    public Vector3 moveVector;
    public Vector3 lastMoveDirection;
    public bool canJump = false;
    public bool noBrake = false;
    public bool moving = false;
    public bool grounded = false;
    public bool dashing = false;
    public float lastJumpTime = Mathf.NegativeInfinity;
    public float lastDashTime = Mathf.NegativeInfinity;
    public Coroutine dashCoroutine;
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();

        being.OnTakeHit += OnTakeHit;
    }

    private void OnDisable()
    {
        being.OnTakeHit -= OnTakeHit;
    }

    private void Start()
    {
        lastMoveDirection = refs.RotatingBody.forward;
    }

    private void Update()
    {
        if (WorldManager.gamePlaying == false)
        {
            ApplyBrake();
            return;
        }

        if (being.aliveMode != AliveMode.Dead)
        {
            AliveUpdate();
        }
        else
        {
            DeadUpdate();
        }
    }
    private void AliveUpdate()
    {
        ComputeGrounded();

        if (master.canPerformActions == false)
        {
            if (grounded == true)
            {
                ApplyBrake();
            }
            return;
        }

        ComputeMoveVector();
        if (moving == true)
        {
            ApplyMoveVector();
            ApplyVelocityLimit();
        }
        else if (grounded == true)
        {
            ApplyBrake();
        }

        if (canJump == true)
        {
            CheckJump();
        }

        if (grounded == true && master.canPerformActions == true)
        {
            if (moving == true && (HasToy == false || HasToy == true && interacts.toyInHand.allowDash))
            {
                CheckDash();
            }
        }
    }
    private void DeadUpdate()
    {
        grounded = false;

        ComputeMoveVector();

        UpdateGhostHeight();

        if (moving == true)
        {
            ApplyMoveVector();
            ApplyVelocityLimit();
        }
        else
        {
            ApplyBrake();
        }
    }
    private void ComputeGrounded()
    {
        RaycastHit hit;
        Physics.SphereCast(GetGroundCheckRay(), refs.Coll.radius * 0.9f, out hit, Mathf.Abs(rayDistance), WorldManager.GroundMask);

        grounded = hit.collider != null;
        if (grounded == true && Time.time > lastJumpTime + jumpCooldown)
        {
            canJump = true;
        }
    }
    private void ComputeMoveVector()
    {
        moveVector =
           (refs.Body.forward * master.moveInput.y +
            refs.Body.right * master.moveInput.x) * 
            (grounded == true ? acceleration : airAcceleration);

        moving = moveVector.sqrMagnitude > 0.01f;

        if (moving == true)
        {
            lastMoveDirection = moveVector.normalized;
        }
    }
    private void UpdateGhostHeight()
    {

        if (Physics.OverlapSphere(Position.AddY(0.2f), 0.2f, WorldManager.GroundMask).Length > 0)
        {
            Velocity = Velocity.SetY(ghostUpDownSpeed);
        }
        else if (master.holdHeight == false)
        {
            RaycastHit hit;
            Physics.Raycast(Position, Vector3.down, out hit, 100f, WorldManager.AbsoluteMask);

            if (hit.collider != null && Position.y - hit.point.y > 0.1f)
            {
                Velocity = Velocity.SetY(-ghostUpDownSpeed);
            }
            else
            {
                Velocity = Velocity.SetY(0);
            }
        }
        else
        {
            Velocity = Velocity.SetY(0);
        }
    }
    private void ApplyMoveVector()
    {
        Velocity += moveVector * Time.deltaTime;
    }
    private void ApplyVelocityLimit()
    {
        float currentMaxSpeed = (HasToy ? normalMaxSpeed * interacts.toyInHand.moveSpeedFactor : normalMaxSpeed);
        if (grounded == true || being.aliveMode == AliveMode.Dead)
        {
            currentMaxSpeed *= master.moveInput.magnitude;
        }

        if (Velocity.SetY(0).magnitude > currentMaxSpeed)
        {
            float y = Velocity.y;
            Velocity = Vector3.MoveTowards(Velocity, (Velocity.SetY(0).normalized * currentMaxSpeed).SetY(y), brakeForce * Time.deltaTime);
        }
    }
    private void ApplyBrake()
    {
        if (noBrake == true)
        {
            return;
        }

        Velocity = Velocity.SubtractMagnitude((being.aliveMode == AliveMode.Dead ? ghostBrakeForce : brakeForce) * Time.deltaTime);
    }
    private void CheckJump()
    {
        if (master.jump == true && Time.time > lastJumpTime + jumpCooldown)
        {
            Jump();
        }
    }
    private void CheckDash()
    {
        if (master.dash == true && Time.time > lastDashTime + dashCooldown)
        {
            Dash();
        }
    }
    #endregion

    #region Coroutines
    private IEnumerator DashCoroutine()
    {
        Vector3 dashDirection = moveVector.normalized;

        float count = 0f;
        while(count < dashDuration)
        {
            Velocity = dashDirection * dashSpeed;

            yield return null;
            count += Time.deltaTime;
        }

        DashEnd();
    }
    #endregion

    #region Functionalities
    private void Jump()
    {
        grounded = false;
        canJump = false;
        lastJumpTime = Time.time;
        Velocity = Velocity.SetY(jumpSpeed);

        OnJump.Invoke();
    }
    private void Dash()
    {
        lastDashTime = Time.time;
        dashing = true;

        if (dashCoroutine != null)
        {
            StopCoroutine(dashCoroutine);
            dashCoroutine = null;
        }

        dashCoroutine = StartCoroutine(DashCoroutine());

        refs.Rb.constraints |= RigidbodyConstraints.FreezePositionY;

        OnDash?.Invoke();
    }
    private void DashEnd()
    {
        dashing = false;

        refs.Rb.constraints &= ~RigidbodyConstraints.FreezePositionY;


        OnDashEnd?.Invoke();
    }
    private void RemoveNoBrake()
    {
        noBrake = false;
    }

    Ray GetGroundCheckRay()
    {
        return new Ray(Position.AddY(startY), Vector3.down);
    }
    #endregion

    #region Events
    public event System.Action OnJump = delegate { };
    public event System.Action OnDash = delegate { };
    public event System.Action OnDashEnd = delegate { };

    private void OnTakeHit(HitInfos infos)
    {
        Velocity = infos.direction * infos.knockbackForce;
        moveVector = Vector3.zero;
        noBrake = true;
        Invoke("RemoveNoBrake", infos.stunTime);
    }
    #endregion

#if UNITY_EDITOR
    #region Editor
    private void OnDrawGizmos()
    {
        if (componentInitialized == false)
        {
            InitializeComponent();
        }

        Gizmos.DrawRay(Position, moveVector.normalized);
        Gizmos.DrawWireSphere(Position.AddY(startY), refs.Coll.radius * 0.9f);
        Gizmos.DrawWireSphere(Position.AddY(startY + rayDistance), refs.Coll.radius * 0.9f);
    }
    #endregion
#endif
}
