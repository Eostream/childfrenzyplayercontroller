﻿/*
Made by: Allan Arnaudin, Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    protected static T Instance;

    public static T getInstance
    {
        get { return Instance; }
    }

    protected virtual void Awake()
    {
        if (Instance)
        {
            Debug.LogError("Already a Singleton ! " + typeof(T).ToString() + " in " + this.gameObject.name + " and i am in " + Instance.gameObject.name);
            Destroy(this.gameObject);
            return;
        }

        Instance = (T)this;
        DontDestroyAtLoading();
    }

    protected virtual void DontDestroyAtLoading()
    {
        DontDestroyOnLoad(this.gameObject);
    }
}