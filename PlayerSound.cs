﻿/*
Made by: Allan Arnaudin, Cossu Michaël
Project: Child Frenzy, GGJ2019.
Date: 1-26-2019
*/

using UnityEngine;

public class PlayerSound : PlayerComponent
{
    #region Fields & Vars
    [SerializeField] private AudioSource source;

    [SerializeField] private AudioClip onJumpClip;
    [SerializeField] private AudioClip onDashClip;

    [SerializeField] private AudioClip onAttackClip;
    [SerializeField] private AudioClip[] onTakeHitClipBoy;
    [SerializeField] private AudioClip[] onTakeHitClipGirl;
    [SerializeField] private AudioClip onTakeHitWithChairClip;
    [SerializeField] private AudioClip[] onDieBoyClip;
    [SerializeField] private AudioClip[] onDieGirlClip;
    [SerializeField] private AudioClip onControlTakenClip;
    [SerializeField] private AudioClip onTakeChairClip;
    #endregion

    #region MonoBehaviour
    protected override void Awake()
    {
        base.Awake();

        moves.OnJump += OnJump;
        moves.OnDash += OnDash;
        being.OnTakeHit += OnTakeHit;
        being.OnDie += OnDie;
        interacts.OnTakeToy += OnTakeToy;
    }

    private void OnDisable()
    {
        moves.OnJump -= OnJump;
        moves.OnDash -= OnDash;
        being.OnTakeHit -= OnTakeHit;
        being.OnDie -= OnDie;
        interacts.OnTakeToy -= OnTakeToy;
    }
    #endregion

    #region Events
    private void OnJump()
    {
        if(onJumpClip)
        {
            if (source.isPlaying)
                source.Stop();
            source.clip = onJumpClip;
            source.Play();
        }
    }
    private void OnDash()
    {
        if (onDashClip)
        {
            if (source.isPlaying)
                source.Stop();
            source.clip = onDashClip;
            source.Play();
        }
    }

    private void OnTakeHit(HitInfos infos)
    {
        source.clip = master.infos.isABoy ? onTakeHitClipBoy[Random.Range(0, onTakeHitClipBoy.Length)] : onTakeHitClipGirl[Random.Range(0, onTakeHitClipGirl.Length)];
        source.Play();

    }
    private void OnDie(HitInfos killingHit)
    {
            if (source.isPlaying)
                source.Stop();

        AudioSource newSource = SceneInfos.PlayerHolder.gameObject.AddComponent<AudioSource>();
        newSource.spatialBlend = 0;
        newSource.clip = master.infos.isABoy ? onDieBoyClip[Random.Range(0, onDieBoyClip.Length)] : onDieGirlClip[Random.Range(0, onDieGirlClip.Length)];
        newSource.Play();
        Destroy(newSource, newSource.clip.length);
    }
    private void OnControlTaken()
    {
        if (onControlTakenClip)
        {
            if (source.isPlaying)
                source.Stop();
            source.clip = onControlTakenClip;
            source.Play();
        }
    }
    private void OnTakeToy()
    {
        if (onTakeChairClip)
        {
            if (source.isPlaying)
                source.Stop();
            source.clip = onTakeChairClip;
            source.Play();
        }
    }
    #endregion
}
